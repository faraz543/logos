<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// ======================== ADMIN ROUTES =============================

Route::group(['prefix' => 'admin','middleware'=>'auth'], function(){
	// ------------------ Admin Panel -------------------------
	// Route::get('/login','Admin\HomeController@login');
	//------------------ ADMIN HOME PAGE -----------------------
	Route::get('/','Admin\HomeController@index');
	//---------------------- LOGO ROUTES ----------------------
	Route::get('/logos', 'Admin\LogoController@index');
	// ------------ CREATING
	Route::get('/logo/new','Admin\LogoController@create');
	Route::post('/logo','Admin\LogoController@store');
	// ------------ UPDATING
	Route::get('/logo/{id}/edit', 'Admin\LogoController@edit');
	Route::post('/logo/edit', 'Admin\LogoController@update');
	// ------------ DELETING
	Route::get('/logo/{id}/del','Admin\LogoController@destroy');

	//------------------ CATEGORIES ROUTES --------------------
	Route::get('/categories', 'Admin\CategoryController@index');
	// ------------ CREATING
	Route::get('/category/new', 'Admin\CategoryController@create');
	Route::post('/category/new', 'Admin\CategoryController@store');
	// ------------ UPDATING
	Route::get('/category/{id}/edit', 'Admin\CategoryController@edit');
	Route::post('/category/edit', 'Admin\CategoryController@update');
	// ------------ DELETING
	Route::get('/category/{id}/del','Admin\CategoryController@destroy');
});
// ====================== ADMIN ROUTES ENDS ===========================

// ======================= MAIN SITE ROUTES ===========================
Route::get('/','SiteController@index');
Route::get('/home','SiteController@index');
Route::get('logo/{id}','SiteController@show');
Route::get('/download/{id}','SiteController@download');
Route::get('/about','SiteController@aboutUs');
Route::get('/contact-us','SiteController@contactUs');
Route::post('/mail','SiteController@mail');

// ======================== FOR WEB USERS =============================
Route::group(['middleware'=>'auth'], function(){
	Route::post('/like','Site\LikeController@store');
	Route::post('/dislike','Site\LikeController@destroy');
	Route::post('/comment','Site\CommentController@store');
	Route::post('/del/comment','Site\CommentController@destroy');
	Route::post('/review','Site\ReviewController@store');
});

