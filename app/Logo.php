<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Logo extends Model {

    use SoftDeletes;

    protected $fillable = ['category_id', 'uploaded_by', 'name', 'url','zip_name','zip_url'];
    
    public function category() {
        return $this->belongsTo('App\Category');
    }
    public function user() {
        return $this->belongsTo('App\User','uploaded_by');
    }
    public function likes(){
    	return $this->morphMany('App\Like','likeable');
    }
    public function comments(){
        return $this->morphMany('App\Comment','commentable')->orderBy('id','desc');
    }
    public function reviews(){
        return $this->morphMany('App\Review','reviewable')->orderBy('id','desc');
    }
}
