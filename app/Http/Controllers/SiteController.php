<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logo;
use App\Category;
use Helpers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logos = Logo::with('category','likes')->get();
        $categories = Category::all();
        // return $logos[0]->likes;
        // return $categories;
        return view('site.index',compact('logos','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return $id;
        $logo = Logo::with('likes','comments','comments.user')->find($id);
        $zip_name = explode('/', $logo->zip_url);
        $zip_name = $zip_name[6];

        if((count(Logo::select('id')->where('category_id',$logo->category_id)->get())) > 8){
            $random_logos = Logo::select('url','id')->where('category_id',$logo->category_id)->get()->random(9);
            $random_logos = array_column(json_decode(json_encode($random_logos), TRUE), 'url');
            $random_logos = (array_chunk($random_logos, 3));
        }
        else{$random_logos  = null;}

        $categories = Category::get();
        $categories = Helpers::getDropDownData($categories);
        $category = $categories[$logo->category_id];
        $like = 'like';

        $logo_count = count(Logo::where('category_id',$logo->category_id)->get());
        $users =  array_column(json_decode(json_encode($logo->likes), TRUE), 'user_id');
        if(Auth::check() && in_array(Auth::user()->id, $users)){
            $like = 'dislike';
        }
        // return $logo->comments[0]->user;
        return view('site.single',compact('like','logo','random_logos','category','logo_count','zip_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function aboutUs(){
        return view('site.about');
    }
    public function contactUs(){
        return view('site.contact');
    }
    public function mail(Request $request){
        // return [$request->email];
        $user = $request->all();
        // return $user['email'];
        $mail = ['first'=>'Uzair543'];
        $i=0;
         \Mail::send('message',compact('user'), function($message) use($request){
                $message->from(env('MAIL_USERNAME'));
                $message->to('talha.chishtie@gmail.com')->subject('Testing')->replyTo($request->email);
            });
    }
}
