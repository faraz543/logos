<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Like;
use App\Logo;
use Validator;

class LikeController extends Controller
{
    protected function likeValidator(array $data){
        return Validator::make($data, [
                        'user_id' => 'required|integer',
                        'likeable_id' => 'required|integer',
                        'likeable_type' => 'required|unique:likes,likeable_type,null,null,user_id,'.$data['user_id'].',likeable_id,'.$data['likeable_id']
                        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->likeValidator($request->all());
        if($validator->fails()){
            echo "refresh";
        }else{
            if(Like::create($request->all())){
                // like successfull so setting the value of like/dislike button equal to dislike
                echo "dislike";    
            }
        }
        // print_r($validator);
        
        
        // 
        // return response()->json($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $record = Like::where('user_id',$request->user_id)->where('likeable_id',$request->likeable_id)->where('likeable_type',$request->likeable_type);
        if(count($record->get()) != 0){
            if($record->delete()){
                // dislike successfull so setting the value of like/dislike button equal to like
                echo 'like';
            }else{
                echo 'refresh';
            }
        }
        else{
            echo 'refresh';
        }
    }
}
