<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Validator;
use Session;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller {

    protected function validatedCategory(array $data) {
        return Validator::make($data, [
                    'name' => 'required|string',
                    'slug' => 'required|string',
        ]);
    }

    public function index() {
        $categories = Category::all();
        return view('category.indexCategory',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('category.createCategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = $this->validatedCategory($request->all());
        // return [$request->all()];
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->messages());
        } else {
            if (Category::create($request->all())) {
                Session::flash('success-category', 'Record Addition Successfull');
                return redirect('/admin/categories');
            } else {
                Session::flash('error-category', 'Record Addition Failed');
                return Redirect::back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return [Category::with('logo')->find($id)];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $category = Category::with('logo')->find($id);
        return view('category.editCategory', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $id = $request->id;
        $record = Category::find($id);
        if ($record) {
            $validator = $this->validatedCategory($request->all());
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator->messages());
            } else {
                if ($record->update($request->all())) {
                    Session::flash('success-category', 'Record Updation Successfull');
                    return redirect('/admin/categories');
                } else {
                    Session::flash('error-category', 'Record Updation Failed');
                    return Redirect::back();
                }
            }
        } else {
            return response()->json(['error' => 'No record found.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $record = Category::find($id);
        if ($record) {
            if ($record->delete()) {
                Session::flash('error-category', 'Record Deletion Successfull');
                return redirect('/admin/categories');
            } else {
                Session::flash('error-category', 'Record Deletion Failed');
                return Redirect::back();
            }
        } else {
            Session::flash('error-category', 'Record Not Found');
        }
    }

}
