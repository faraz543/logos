<?php
namespace App\Http\Controllers\Admin;

use App\Logo;
use App\Category;
use Validator;
use Session;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Helpers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class LogoController extends Controller {

    protected function validatedLogo(array $data,$method) {
        if($method == 'create'){
            return Validator::make($data, [
                        'name' => 'required|string',
                        'category_id' => 'required|integer',
                        'uploaded_by' => 'required|integer',
                        'logo' => 'required|file',
                        'zip' => 'required|file',
            ]);
        }
        if($method == 'update'){
            return Validator::make($data, [
                'name' => 'required|string',
                'category_id' => 'required|integer',
                'uploaded_by' => 'required|integer',
            ]);
        }
    }

    public function index() {
        $logos = Logo::with('user','category')->get();
        // return [$logos];
        return view('logo.indexLogo',compact('logos'));
    }

    public function create() {
        $category = Helpers::getDropDownData(Category::all());
        return view('logo.createLogo', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->merge(['uploaded_by'=> Auth::user()->id]);
        $validator = $this->validatedLogo($request->all(),'create');
        if ($validator->fails()) {
            // return 'fail';
            return Redirect::back()->withErrors($validator->messages()) ;
        } else {
            // getting logo & Zip file name
            $logo = $request->logo;
            $logo_name = $logo->getClientOriginalName();
            $zip = $request->zip;
            $zip_name = $zip->getClientOriginalName();
            $path = $request->category_id.'/';
            // setting image & zip url
            $url = env('IMAGE_URL').$request->category_id.'/'.$logo_name;
            // return $url;
            $zip_url = env('ZIP_URL').$request->category_id.'/'.$zip_name;
            // return $zip_url;
            // merging required files
            $request->merge(['url' => "{$url}",'zip_url' => "{$zip_url}",'zip_name' => "{$zip_name}"]);
            // return $request->all();
            if (Logo::create($request->all())) {
                // Storing logo and ZIP
                $logo_storage = Storage::disk('logo')->put($path.$logo_name,File::get($logo));
                $zip_storage = Storage::disk('zip')->put($path.$zip_name,File::get($zip));
                if ($logo_storage && $zip_storage){
                    Session::flash('success-logo', 'Record Addition Successfull');
                    return redirect('/admin/logos');
                }else{
                    Session::flash('error-logo', 'Uploading failer');
                    return Redirect::back();
                }
            } else {
                Session::flash('error-logo', 'Record Addition Failed');
                return Redirect::back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return [Logo::with('category')->find($id)];
        $logos = Logo::where('category_id',1)->get()->random(3);
    }

    public function edit($id) {
        $logo = Logo::find($id);
        $categories = Helpers::getDropDownData(Category::all());
        return view('logo.editLogo', compact('logo','categories'));
    }

    public function update(Request $request) {
        $id = $request->id;
        $request->merge(['uploaded_by'=> Auth::user()->id]);
        $record = Logo::find($id);
        if ($record) {
            // Validation
            $validator = $this->validatedLogo($request->all(),'update');
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator->messages()) ;
            } else {
                // updateing category and name only
                if ($record->update($request->all())) {
                    Session::flash('success-logo', 'Record Updation Successfull');
                    return redirect('/admin/logos');
                } else {
                    Session::flash('error-logo', 'Record Updation Failed');
                    return Redirect::back();
                }
            }
        } else {
            Session::flash('error-logo', 'No Record Found');
        }
    }

    public function destroy($id) {
        $record = Logo::find($id);
        if ($record) {
            if ($record->delete()) {
                Session::flash('error-logo', 'Record Deletion Successfull');
                return redirect('/admin/logos');
            } else {
                Session::flash('error-logo', 'Record Deletion Failed');
                return Redirect::back();
            }
        } else {
            Session::flash('error-category', 'Record Not Found');
        }
    }

}
