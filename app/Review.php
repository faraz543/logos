<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model {
	protected $fillable = ['user_id','reviewable_id','reviewable_type','body'];

	public function reviewable(){
		return $this->morphTo();
	}
	public function user(){
		return $this->belongsTo('App\User','user_id');
	}
}