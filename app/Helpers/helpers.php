<?php

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Logo;
use App\Category;

class Helpers {


    public static function getDropDownData($data) {
        return array_column(json_decode(json_encode($data), TRUE), 'name', 'id');
    }
    
    public static function logoById($id){
    	return Logo::find($id);
    }
}
