@extends('includes.template')
@section('extra-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.css" integrity="sha256-ke5yDzwl7GsgnYgBnCDiWSNA/x/hyU89VDHl/R535dw=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" integrity="sha256-Tcd+6Q3CIltXsx0o/gYhPNbEkb3HJJpucOvQA7csVwI=" crossorigin="anonymous" />
<style type="text/css">
  #owl-demo .item img{
    display: block;
    width: 100%;
    height: auto;
  }
  .low-padding{
      padding-left: 10%;
  }
  .med-padding{
      padding-left: 20%;
  }
  .high-padding{
      padding-left: 30%;
  }
  .slide1 h2{
  font-family: Comic Sans MS;
  }
  .slide1 ul{
      text-align: left;
      margin-top: 160px;
  }
  .slide2 h2{
      color: #fff;
      font-family: Comic Sans MS;
  }
  .slide2 ul{
      text-align: center;
      margin-top: 160px;
  }
  .slide3 ul{
      text-align: center;
      margin-top: 200px;
  }
</style>
@endsection
@section('content')
<section class="big" id="home" >
    <div id="owl-demo" class="owl-carousel owl-theme custom1">
      <div class="item">
        <div class="col-md-12 slide1 floatleft">
          <div class="col-md-6 no-padding floatleft bounceIn animated">
                <img src="img/98.gif" >
            </div>
            <div class="col-md-6 no-padding floatleft ">
                <ul>
                  <li class="bounceInLeft animated low-padding"><h2 style="color:#ff217b;">Perfect Color.</h2></li>  
                  <li class="rubberBand animated med-padding"><h2 style="color:#fff68d;">Unique Designs.</h2></li>  
                  <li class="bounceInRight animated high-padding"><h2 style="color:#befff7;">Perfect Contrast.</h2></li>  
                </ul>
            </div>
        </div>
        </div>
      <div class="item">
        <div class="col-md-12 slide2 floatleft">
            <div class="col-md-6 no-padding floatleft ">
                <ul >
                  <li class="low-padding"><h2>Creative.</h2></li>  
                  <li class="med-padding"><h2>&nbsp;&nbsp;&nbsp;Innovative.</h2></li>  
                  <li class="high-padding"><h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Design.</h2></li>  
                </ul>
            </div>
            <div class="col-md-6 no-padding floatleft bounceIn animated">
                <img src="img/45.gif" >
            </div>
        </div>
      </div>
      <div class="item">
        <div class="col-md-12 slide3 floatleft">
          <div class="col-md-6 no-padding floatleft bounceIn animated">
                <img src="img/download.gif" >
            </div>
            <div class="col-md-6 no-padding floatleft ">
                <ul>
                  <li><h2>
                      Download Your Free Logo NOW!
                  </h2></li>  
                </ul>
            </div>
        </div>
      </div>

    </div>
</section>
<div class="small-padding" id="portfolio">
    <div class="wrapper">
        <div id="filters" class="col-md-12">
            <div class="less-width tcenter">
                <a href="*" class="active button">All</a>
                @foreach($categories as $category)
                    <a href=".{{ $category->slug }}" class="button">{{ $category->slug }}</a>
                @endforeach
            </div>
        </div>
        <div class="clear"></div><!--CLEAR FLOATS-->
    </div>
</div>
<div class="dark-wrapper overlay less-width">
    <div class="wrapper col-md-12">

        <div id="loader"></div>

        <ul class="clearfix portfolio-isotope portfolio scroll-animate bottom">

            @foreach($logos as $logo)
            <li class="{{$logo->category->slug}}">
                <a href="logo/{{$logo->id}}" class="isotope-alt-image">
                    <img src="{{ $logo->url }}" class="logo-site" alt="image" height="285px" width="405px"/>
                    <div>
                        <h4>{{$logo->name}}<small>{{$logo->category->slug}}</small></h4>
                        <p><i class="fa fa-heart"></i> {{ count($logo->likes) }}  </p>
                    </div>
                </a>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">{{$logo->name}}</h4>
                        <p class="meta">{{$logo->category->slug}}</p>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
        <!-- <a href="more-posts.html" id="load-more" class="scroll-animate top">Load More</a> -->
    </div>

</div>
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js" integrity="sha256-kiFgik3ybDpn1VOoXqQiaSNcpp0v9HQZFIhTgw1c6i0=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var owlCarousel = $('.owl-carousel');
        owlCarousel.owlCarousel({
          slideSpeed : 900,
          paginationSpeed : 900,
          singleItem:true,
          autoPlay : 3000,
          addClassActive : true,
          beforeMove: function(){
            this.$owlItems.removeClass('slideInUp animated');
            this.$owlItems.removeClass('slideOutUp animated');
            this.$owlItems.addClass('slideOutUp animated') 
          },
          afterMove: function(){
            this.$owlItems.eq(this.currentItem).removeClass('slideOutUp animated') 
            this.$owlItems.eq(this.currentItem).removeClass('slideInUp animated');
            this.$owlItems.eq(this.currentItem).addClass('slideInUp animated') 
          },
        });
        $('.hover').hover(function(){
          $(this).addClass('wobble animated'); 
        });
    });
</script>
@endsection
