@extends('includes.template')

@section('content')
	<section class="big overlay" id="home" style="height: 200px;">
		<h1 class="mega">ABOUT US</h1>
	</section>
	
	<section class="dark-wrapper">
	
		<div class="wrapper">
		
			<div class="col-md-12">
					<a href="single.html" class="isotope-alt-image">
						<img src="img/columns.jpg" alt="image" />
						<div>
							<h4 class="light">See the Features
							<small>By Tommus, on 12 July 2988, tagged as Tommus, Design, 3 Comments</small>
							</h4>
						</div>

					</a>
					<div class="overlay padding">
						<h3><a href="single.html" class="light">See the Features</a></h3>
						<h6>The Subtitle. Morbi non erat non ipsum pharetra tempus. Donec orci. Proin in ante. Pellentesque sit amet purus. Cras egestas diam sed ante.</h6>
						<p>Etiam imperdiet urna sit amet risus. Donec ornare arcu id erat. Aliquam ultrices scelerisque sem. In elit nulla, molestie vel, ornare sit amet, interdum vel, mauris. Etiam dignissim imperdiet metus. In elit nulla, molestie vel, ornare sit amet, interdum vel, mauris. Etiam dignissim imperdiet metus.</p>
					</div>
			</div>
			<div class="clear"></div><!--CLEAR FLOATS-->
			
		</div>
		
	</section>
@endsection