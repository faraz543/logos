@extends('includes.template')
@section('extra-css')
    <link rel="stylesheet" type="text/css" href="/css/custom.css" />
    <style type="text/css">
    	.like-section{
    		padding: 15px 30px;
    		display: flex;
    	}
    	.like-dislike,.comment,.comment-button,.review-button{
    		text-transform: capitalize;
    		color: #fff!important;
		    background-color: transparent!important;
		    border: 1px solid #fff!important;
		    border-radius: 0px!important;
		    font-size: 16px;
		    padding-left: 15px;
		    padding-right: 15px;
		    margin-right: 15px;
		    box-shadow: none!important;
    	}
    	.like-dislike:hover,.like-dislike:focus{
    		background-color: transparent!important;
    		border: 1px solid #fff!important;
    		box-shadow: none!important;
    	}
    	.like-section > p{
    		margin:8px;
    	}
    	.comment-section,.review-section{
    		padding: 15px 30px;
    	}
    	#comment-form,#review-form{
    		padding: 15px;
    		padding-left: 0px;
    	}
    	#comment_id{
    		background: rgba(41, 85, 127,0)!important;
    		border:1px solid #fff; 
		    border-radius: 0px;
		    box-shadow: none;
		    color: #fff;
    	}
    	#comment_id:focus{
    		background: rgba(41, 85, 127,0.65)!important;
    		border:1px solid #fff; 
    	}
    	.content{
    		padding: 15px 30px!important;
    	}
    	.all-comments {
		    padding: 10px;
		    border: 1px solid #fff;
		    margin-top: 30px;
		    background: rgba(41, 85, 127,0.65)!important;
		    box-shadow: 2px 3px 25px #272727;
		}
		.single-comment{
		    text-transform: capitalize;
		    padding: 10px;
		    margin-bottom: 5px;
		}
		.current-user{
			background-color: rgba(18, 31, 50, 0.5);
		}
		.all-comments > .col-md-12{
			display: flex;
		}
		#reviewModal .modal-content{
			background-color: rgba(29, 70, 108, 0.75)!important;
			border: 1px solid #fff!important;
			color: #fff!important;
		}
    </style>
@endsection
@section('content')
	<section class="big overlay" id="home">
		<h1 class="mega single">{{$logo->name}}<br /><small>Published On: {{$logo->created_at}}</small></h1>
	</section>
	<section class="dark-wrapper">
		<div class="wrapper overlay">
			<div class="col-md-12">
      				<div class="col-md-8 center header">
		            	<img  src="{{$logo->url}}" class="single-logo">
				    </div>
				    <div id="user-action" class="col-md-12">
				    	<!-- LIKE SECTION -->
				    	<div class="like-section">
      						{!! (Auth::check() ? "<a class='btn btn-primary like-dislike' href='' id='$like'>$like</a>" : '' ) !!}
							<p><span id="total">{{count($logo->likes)}}</span>&nbsp;&nbsp;<i class="fa fa-heart"></i></p>
						</div>
      					<!-- COMMENT SECTION -->
      					<div class="comment-section">
      						@if(Auth::check())
		      					<a class="btn btn-primary comment-button" href="" id="">Comment</a>
		      					<!-- FORM OPEN (COMMENT) -->
		      					{!! Form::open(['method'=>'POST','action'=>'Site\CommentController@store','id'=>'comment-form','class'=>'form-horizontal','style'=>'display:none']) !!}
		      						<div class="form-group">
		      							{!! Form::label('comment','Comment',['id'=>'label-id','class'=>'label-class no-padding col-sm-3 control-label']) !!}
		      							<div class='col-sm-6 no-padding'>
		      								{!! Form::textarea('comment',null,['class'=>'class form-control','id'=>'comment_id','placeholder'=>'Your Comment','style'=>'height:100px']) !!}
		      							</div>
		      							<div class="col-sm-3 no-padding">
		      							{!! Form::submit('Post',['class'=>'btn btn-primary comment']) !!}
		      							</div>
		      						</div>
		      					{!! Form::close() !!}
		      					<!-- FORM CLOSE (COMMENT) -->
      						@endif
      						@if(count($logo->comments) == 0)
      							<style type="text/css">
      								.all-comments{
      									display: none!important;
      								}
      							</style>
      						@endif
      						<div class="all-comments">
	      						@foreach($logo->comments as $comment)
	      						<div class="col-md-12 single-comment {{($check = Auth::check() && $comment->user->id == Auth::user()->id ? 'current-user' : '')}}">
		      						<div class="col-md-11">
			      						<p style="margin:0px"><b>{{$comment->user->name}}</b>  : {{$comment->body}}
			      						</p>
		      						</div>
		      						<div class="col-md-1">
		      							{!!($check ? "<strong style='text-align: right'><a class='btn btn-danger btn-sm' val='$comment->id' id='comment-del' href=''><i class='fa fa-remove'></i></a></strong>" : '')!!}
		      						</div>
	      						</div>
	      						@endforeach
      						</div>
      					</div>
      					<!-- SECTION END(COMMENT) -->
      					<!-- REVIEW SECTION -->
      					<div class="review-section">
      						@if(Auth::check())
      						<button type="button" class="btn btn-info review-button" data-toggle="modal" data-target="#reviewModal">Review</button>      						
      						
      						@endif
      					</div>
      					<!-- SECTION END(REVIEW) -->
      				</div>
					<div class="content col-md-12">
						<div class='col-md-12'>
			            <div class="break15"></div>
			            <h4>There's loads of other Logos on our sites.</h4>
			            <p>There are curently {{$logo_count-1}} more logos in {{$category}} </p>
			            
			            <h5>NAME</h5>
			            <p>{{$logo->name}}</p>
			            <hr />

			            <h5>CATEGORY</h5>
			            <p>{{$category}}</p>
			            <hr />

			            <h5>CONTENT OF DOWNLOAD FILE</h5>
			            <ul>
			            	<li>PSD FILE</li>
			            	<li>JPG FILE</li>
			            	<li>PDF FILE</li>
			            </ul>
			            <hr />

			            </div>
			            <div class="col-md-12">
				          	<ul class="rslides recent-logo">
				          		@if($random_logos != null)
					            	@foreach ($random_logos as $key => $value) 
							            <li>
								            <img src = '{{$value[0]}}' alt="image" class='col-md-4 ran-logo'>
								            <img src = '{{$value[1]}}' alt="image" class='col-md-4 ran-logo'>
								            <img src = '{{$value[2]}}' alt="image" class='col-md-4 ran-logo'>
							            </li>
							    	@endforeach
						    	@endif
				          	</ul>
			          	</div>
			          	<div class="col-md-12 download-div">
			          	<h5>Download link</h5>
			            <a href="" id="countdown" class="btn btn-primary download" >Download Now</a>
			            </div>
    				</div>
    		</div>
    	</div>
    	<!-- Review Modal -->
    	@if(Auth::check())
		<div id="reviewModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove" style="color: #fff"></i></button>
		      </div>
		      <div class="modal-body">
		        {!! Form::open(['method'=>'POST','action'=>'Site\ReviewController@store','id'=>'review-form','class'=>'form-horizontal']) !!}
					<div class="form-group">
						{!! Form::label('review','Review',['id'=>'label-id','class'=>'label-class no-padding col-sm-3 control-label']) !!}
						<div class='col-sm-6 no-padding'>
							{!! Form::textarea('body',null,['class'=>'class form-control','id'=>'review_id','placeholder'=>'Your Review','style'=>'height:100px']) !!}
							{!! Form::hidden('user_id',Auth::user()->id) !!}
							{!! Form::hidden('reviewable_id',"$logo->id") !!}
						</div>
						<div class="col-sm-3 no-padding">
						{!! Form::submit('Post',['class'=>'btn btn-primary comment']) !!}
						</div>
					</div>
				{!! Form::close() !!}
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
		@endif
    </section>
@endsection
@section('extra-js')
<script type="text/javascript">
	document.getElementById('countdown').onclick = function() {
		event.preventDefault();
    	var countdownElement = document.getElementById('countdown'),
        downloadButton = document.getElementById('download'),
        seconds = 5,
        second = 0,
        interval;

	    interval = setInterval(function() {
	        countdownElement.innerHTML = 'Wait ' + (seconds - second) + ' Downloading will start automatically';
	        if (second >= seconds) {
	        	window.location = '{{$logo->zip_url}}';
	        	countdownElement.innerHTML = 'Download Again';
	            clearInterval(interval);
	        }

	        second++;
	    }, 1000);
	};
</script>
@if(Auth::check())
	<script type="text/javascript">
		var user_id = "{{Auth::user()->id}}";
		var logo_id = "{{$logo->id}}";
		var model_type = "App\\Logo";
		// SINGLE AJAX CALL FOR BOTH LIKE/DISLIKE
		$(document).on('click', '.like-dislike', function() {
			event.preventDefault();
			var link = $('.like-dislike').attr('id');
			$.ajax({
				url:"/"+link,
				method:'POST',
				data:{user_id: user_id,likeable_id: logo_id,likeable_type: model_type,'_token':'{!! csrf_token() !!}'},
				success: function(data){
					if(data == 'refresh'){
						location.reload();
					}else{
						$('.like-dislike').attr('id',data);
						$('.like-dislike').html(data);
						if(data == 'dislike'){
							here = parseInt($('#total').html());
							$("#total").html(here+1)
						}
						if(data == 'like'){
							here = parseInt($('#total').html());
							$("#total").html(here-1)
						}
					}
				}
			})
		});

		// APPEARING COMMENT SECTION
		$('.comment-button').on('click',function(event){
			event.preventDefault();
			if($("#comment-form").is(":visible")){
				$('#comment-form').hide();
			}else{
				$('#comment-form').show();
			}
			// $('#comment-form').show();
		});

		// AJAX CALL FOR COMMENTING
		$(document).on('submit', '#comment-form', function() {
			var comment_body = $('#comment_id').val();
			event.preventDefault();
			$.ajax({
				url:"/comment",
				method:'POST',
				data:{user_id: user_id,commentable_id: logo_id,commentable_type: model_type,body:comment_body,'_token':'{!! csrf_token() !!}'},
				success: function(data){
					if(data == 'refresh'){
						location.reload();
					}else{
						$('#comment-form').each(function(){
						    this.reset();
						});
						if($.isNumeric(data)){
							$('.all-comments').show();
							$("<div class='col-md-12 single-comment current-user'><div class='col-md-11'><p style='margin:0px'><b>{{Auth::user()->name}} : </b>"+ comment_body +"</p></div><div class='col-md-1'><strong style='text-align: right'><a class='btn btn-danger btn-sm' val=" + data +" id='comment-del' href=''><i class='fa fa-remove'></i></a></strong></div></div>").prependTo('.all-comments');	
						}
						// alert(data);
					}
				}
			});
		});

		// COMMENT DELETE AJAX
		$(document).on('click', '#comment-del', function(event){
			event.preventDefault();
			var id = $(this).attr('val');
			$.ajax({
				url:"/del/comment",
				method:'POST',
				data:{id:id,'_token':'{!! csrf_token() !!}'},
				success: function(data){
					console.log(data);
					if($.isNumeric(data)){
						$('[val='+data+']').closest('div.single-comment').remove();
						if ($('.all-comments').children().length == 0){
							$('.all-comments').hide();
						}
					}else{
						location.reload();
					}
				}
			})
		});

		// APPEARING REVIEW SECTION
		$('.review-button').on('click',function(event){
			event.preventDefault();
		});
	</script>
@endif
@endsection