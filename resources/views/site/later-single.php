<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
	  <div class="one carousel-item">
	  	<div class="col-md-9 center">
	    	<img class="contrast" src="{{$logo->url}}" alt="First slide">
				<div class="carousel-caption">
					<h2 class="shade">Dark Shade</h2>
	   		</div>
	    </div>
	  </div>
	  <div class="two carousel-item active">
	    <div class="col-md-9 center">
	    	<img class="" src="{{$logo->url}}" alt="First slide">
	    	<div class="carousel-caption">
					<h2 class="shade">Original</h2>
	   		</div>
	    </div>
	  </div>
	  <div class="three carousel-item">
	    <div class="col-md-9 center">
	    	<img class="saturate" src="{{$logo->url}}" alt="First slide">
	    	<div class="carousel-caption">
					<h2 class="shade">Bright Shade</h2>
	   		</div>
	    </div>
	    <div class="container">
	    </div>
	  </div>
	  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
	  D<br>A<br>R<br>K<br>E<br>R
	</a>
	<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
	  B<br>R<br>I<br>G<br>H<br>T<br>E<br>R
	</a>
	  </div>
</div>