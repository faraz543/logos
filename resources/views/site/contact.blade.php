@extends('includes.template')

@section('content')

	<section class="big overlay margin-bottom" id="home" style="height: 200px;">
		<h1 class="mega">CONTACT US</h1>
	</section>
	<div class="margin-bottom">
	<section class="overlay less-width">
	
		<div class="wrapper clearfix">
			
			<div class="two_thirds scroll-animate left">
				{!! Form::open(['method'=>'post','action' => 'SiteController@mail','id'=>'contactform','class'=>'form-horizontal']) !!}
				{!! Form::text('name',null,['class'=>'class form-control','id'=>'name','placeholder'=>'Name']) !!}
				{!! Form::email('email',null,['class'=>'class form-control','id'=>'email','placeholder'=>'Email']) !!}
				{!! Form::textarea('message',null,['class'=>'class form-control','id'=>'message','placeholder'=>'Your Message']) !!}
				{!! Form::submit('Send Message',['']) !!}
				{!! Form::close() !!}
			</div>
			
			<div class="one_third last scroll-animate right">
				<h5>Contact Us</h5>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar.</p>
				<p class="margin-bottom-10"><i class="fa fa-map-marker fa-fixed-width"></i> York, England</p>
				<p class="margin-bottom-10"><i class="fa fa-phone-square fa-fixed-width"></i> +00 (123) 456 78 90</p>
				<p class="margin-bottom-10"><i class="fa fa-laptop fa-fixed-width"></i> <a href="first.last@email.com">first.last@email.com</a></p>
			</div>
			
		</div>
		
	</section>
	</div>

@endsection