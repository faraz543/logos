@extends('includes.template')
@section('content')
<div class="small-padding" id="portfolio">

    <div class="wrapper">
        <h3 class="floatleft">Our Work <small class="portfolio-filter">We're really proud.</small></h3>

        <div id="filters" class="floatright">
            <a href="*" class="active button">All</a>
            <a href=".website" class="button">Website</a>
            <a href=".IT" class="button">IT</a>
            <a href=".sports" class="button">Sports</a>
            <a href=".hotel" class="button">Hotel</a>
            <a href=".art" class="button">Art</a>
            <a href=".business" class="button">Business</a>
        </div>

        <div class="clear"></div><!--CLEAR FLOATS-->
    </div>

</div>
<div class="dark-wrapper">

    <div class="wrapper overflow-hidden">

        <div id="loader"></div>

        <ul class="clearfix portfolio-isotope portfolio scroll-animate bottom">

            <li class="website">
                <a href="single-portfolio.html" class="isotope-alt-image">
                    <img src="/img/3i.jpg" alt="image" height="285px" width="405px"/>
                    <div>
                        <h4>Project title<small>WebSite</small></h4>
                    </div>
                </a>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">WebSite</p>
                    </div>
                </div>
            </li>
            <li class="IT">
                <a href="single-portfolio.html" class="isotope-alt-image">
                    <img src="/img/4i.jpg" alt="image" height="285px" width="405px"/>
                    <div>
                        <h4>Project title<small>Web Design, Development</small></h4>
                    </div>
                </a>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
            </li>
            <li class="sports">
                <a href="single-portfolio.html" class="isotope-alt-image">
                    <img src="/img/ii.jpg" alt="image" height="285px" width="405px" />
                    <div>
                        <h4>Project title<small>Web Design, Development</small></h4>
                    </div>
                </a>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
            </li>
            <li class="hotel">
                <a href="single-portfolio.html" class="isotope-alt-image">
                    <img src="/img/i2.jpg" alt="image" height="285px" width="405px" />
                    <div>
                        <h4>Project title<small>Web Design, Development</small></h4>
                    </div>
                </a>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
            </li>
            <li class="art">
                <a href="single-portfolio.html" class="isotope-alt-image">
                    <img src="/img/i3.jpg" alt="image" />
                    <div>
                        <h4>Project title<small>Web Design, Development</small></h4>
                    </div>
                </a>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
            </li>
            <li class="business">
                <a href="single-portfolio.html" class="isotope-alt-image">
                    <img src="/img/i4.jpg" alt="image" />
                    <div>
                        <h4>Project title<small>Web Design, Development</small></h4>
                    </div>
                </a>
                <div class="isotope-alt-details">
                    <div>
                        <h4 class="remove-bottom">Project title</h4>
                        <p class="meta">Web Design, Development</p>
                    </div>
                </div>
            </li>
        </ul>

        <!-- <a href="more-posts.html" id="load-more" class="scroll-animate top">Load More</a> -->

    </div>

</div>
@endsection

