@extends('layouts.app')
@section('extra-css')
<style>
    .diba {
        width: 80%;
        height: 150px;
        border: 1px solid #CCCFD2;
        border-radius: 10px;
        margin-left: 10%;
    }
    .add-button{
        margin:15px 0px;
    }

</style>
@endsection
@section('content')
@include('includes.flashmessage')
<div class="container">
    <div class="row">
        @include('includes.sidebar')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Categories</div>
                {{ message('category') }}                
                <div class="col-md-12"> 
                    <a href="{!! url('admin/category/new')!!}" class="btn btn-primary add-button">Add Categories &nbsp;<i class="fas fa-plus"></i></a>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered text-center">
                        <thead>
                            <tr>
                                <th width="40%" class="text-center">Category Name</th>
                                <th width="20%" class="text-center">Slug</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td>{{$category->name}}</td>
                                        <td>{{$category->slug}}</td>
                                        <td>
                                            <button><a href='{{ url("admin/category/$category->id/edit") }}'>Edit&nbsp;&nbsp;&nbsp;<i class="fas fa-pencil-alt"></i></a></button>
                                            <button class="delete" val="{{$category->id}}">Delete&nbsp;&nbsp;&nbsp;<i class="fas fa-trash-alt delete"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                <!-- <td>
                                    <a href=""> <i class="fas fa-pencil-alt"></i></a>
                                    <a> <i class="fas fa-trash-alt delete"></i></a>
                                </td> -->
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="delete-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation!</h4>
            </div>
            <div class="modal-body">
                <p>Are You sure you want To delete this record?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href='' type="button" id="del-id" class="btn btn-danger">Delete</a>
            </div>
        </div>

    </div>
</div>
@endsection
@section("extra-js")
<script>
    $('.delete').on("click", function () {
        $('#delete-modal').modal('show');
        var id = ($(this).attr('val'));
        $('#del-id').attr('href','category/'+id+'/del')
    });
</script>
@endsection