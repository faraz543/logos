@extends('layouts.app')
@section('content')
@include('includes.flashmessage')
<div class="container">
    <div class="row">
        @include('includes.sidebar')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Add Category</div>
                {{ count($errors) == 0 ? '' : fielderrors($errors) }}
                <div class="panel-body">
                    {!! Form::open(['method'=>'post','action'=>'Admin\CategoryController@store','id'=>'form-id','class'=>'form-horizontal']) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                            {!! Form::text('name',null,['class'=>'class form-control','id'=>'id']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('category_id','Slug',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                           {!! Form::text('slug',null,['class'=>'class form-control','id'=>'id']) !!}
                            </div>
                        </div> 
                        <div class="form-group">
                            {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                            {!! Form::submit('Add',['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
