@extends('layouts.app')
@section('extra-css')
<style>
    .diba{
        width:100%;
        height:200px;
        border:1px solid #CCCFD2;
        border-radius: 10px;
        margin-bottom: 15px;
    }
   
</style>
@endsection
@section('content')
<div class="container">
    <div class="row">
        @include('includes.sidebar')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Category</div>

                <div class="panel-body">
                   
                    <div class="clearfix"></div>
                    {!! Form::open(['method'=>'post','action' => 'Admin\CategoryController@update' ,'id'=>'form-id','class'=>'form-horizontal', 'files' => true]) !!}


                    <div class="form-group">
                        {!! Form::label('name', 'Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::text('name',"$category->name",['class'=>'class form-control','id'=>'id']) !!}
                        </div>
                    </div>
                      <div class="form-group">
                        {!! Form::label('name', 'Slug',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::text('slug',"$category->slug",['class'=>'class form-control','id'=>'id']) !!}
                            {!! Form::hidden('id',"$category->id") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                        <div class="col-sm-6"> 
                            {!! Form::submit('EDIT',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section("extra-js")
<script>
    $('.delete').on("click", function () {
        $('#delete-modal').modal('show');
    });
//    $('#delete-modal').modal('show');
</script>
@endsection