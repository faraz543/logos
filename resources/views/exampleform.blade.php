@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add</div>

                <div class="panel-body">
                    <!-- <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form> -->

                    {!! Form::open(['method'=>'post','id'=>'form-id','class'=>'form-horizontal']) !!}

                        <div class="form-group">
                            {!! Form::label('name', 'Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                            {!! Form::text('name',null,['class'=>'class form-control','id'=>'id']) !!}
                            </div>
                        </div>
<!--
                        <div class="form-group">
                            {!! Form::label('for',
                            'YOUR PASSWORD',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                            {!! Form::password('password-name',['class'=>'password-class form-control']) !!}
                            </div>
                        </div>-->
                        <div class="form-group">
                            {!! Form::label('category_id','Category',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                           {!! Form::select('category_id',['1'=>"Website",'2'=>'IT','3'=>'Sports','4'=>'Hotel'],null,['class'=>'form-control']) !!}
                            </div>
                        </div> 
                        <div class="form-group">
                            {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                            {!! Form::submit('Add',['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
