<?php 
function message($var){ ?>
    <div class="col-md-12" style="padding-top: 15px;">
        @if (Illuminate\Support\Facades\Session::has("success-{$var}"))
            <div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                {{ session("success-{$var}") }}
            </div>
        @elseif(Illuminate\Support\Facades\Session::has("error-{$var}"))
                <div class='alert alert-danger alert-dismissible' role='alert'>
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                    {{ session("error-{$var}") }}
                </div>
        @endif	   
    </div>
<?php }
function fielderrors($errors){?>
    <div class="col-md-12" style="padding-top: 15px;">
        <div class='alert alert-danger alert-dismissible col-md-12' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            <ul>
                @php
                    foreach ($errors->all() as $error) {
                        echo "<li>$error</li>";
                    }
                @endphp
            </ul>
        </div>
    </div>
<?php } ?>