<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Tides - HTML Template</title>
    <meta name="author" content="" />
    <meta name="description" content="" />
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/framework.css" />
    <link rel="stylesheet" type="text/css" href="/css/plugins.css" />
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <link rel="stylesheet" type="text/css" href="/css/site.css" />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/animate.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>

	<div class="header">
		<div class="wrapper clearfix fleft">
			<div class="one_fourth remove_bottom logo">
				<a href="index.html">
					<h1 class="logo">LOGO POOL</h1>
				</a>
			</div><!--/one_fourth-->
			<div class="three_fourths remove_bottom last">
				<nav>
					<ul id="selectnav">
						@php
							$pages = [ url('/') => 'HOME', url('/about') => 'ABOUT US', url('contact-us') => 'CONTACT US' ];
							$current = url(Request::path());
						@endphp
						@foreach($pages as $href => $name)
						<li>
							<a href="{{$href}}" class= "scroller
							{{(($href == $current) ? 'active' : '')}}
							 ">{{$name}}</a>
						</li>
						@endforeach
					</ul>
				</nav>
			</div><!--/three_fourths-->
		</div><!--/wrapper-->
	</div><!--/div-->