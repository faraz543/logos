@include ('includes.header')

@yield('extra-css')

@yield('content')

@include ('includes.footer')

@yield('extra-js')