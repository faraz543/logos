<div class="col-md-3">
            <!-- SIDE BAR START -->
            <div class="panel panel-default">
                <div class="panel-heading">Pages</div>
                <div class="panel-body no-padding">
                    <div class="block-content">
                        <?php
                            $current = Request::path();
                            $pages = [ 'admin'=>'HOME','admin/logos' => 'LOGO','admin/categories' => 'CATEGORIES', ];
                            echo "<ul class='sidebar no-padding'>";?>
                                @foreach ($pages as $href => $page) 
                                    <li class = 
                                    "{{ ($current == $href) ? 'active' : '' }}"
                                    ><a href='/{{$href}}'>{{$page}}</a></li>
                                @endforeach
                            <?php echo "<ul/>";
                        ?>
                    </div>
                 </div>
            </div>
            <!-- END -->
        </div>