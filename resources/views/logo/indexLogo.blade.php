@extends('layouts.app')
@section('extra-css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
@endsection
@section('content')
@include('includes.flashmessage')
<div class="container">
    <div class="row">
        @include('includes.sidebar')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Logos</div>
                {{ message('logo') }}
                <div class="col-md-12"> 
                    <a href="{!! url('admin/logo/new')!!}" class="btn btn-primary add-button">Add Logos <i class="fas fa-plus"></i></a>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered text-center display" id="example" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th width="40%" class="text-center">Logo</th>
                                <th width="20%" class="text-center">Uploaded By</th>
                                <th width="20%" class="text-center">Category</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1; @endphp
                            @foreach($logos as $logo)
                            <tr>
                                <td>{{ $logo->id }}</td>
                                <td><div class="logo-display"><img src="{{ $logo->url }}"></div></td>
                                <td>{{ $logo->user->name }}</td>
                                <td>{{ $logo->category['name'] }}</td>
                                <td><button class="delete" val="{{$logo->id}}">Delete&nbsp;&nbsp;&nbsp;<i class="fas fa-trash-alt delete"></i></button></td>
                            </tr>
                            @endforeach
                            <tfoot>
                                <tr>
                                    <th class="text-center">Id</th>
                                    <th width="40%" class="text-center">Logo</th>
                                    <th width="20%" class="text-center">Uploaded By</th>
                                    <th width="20%" class="text-center">Category</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="delete-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation!</h4>
            </div>
            <div class="modal-body">
                <p>Are You sure you want To delete this record?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="" type="button" id="del-id" class="btn btn-danger">Delete</a>
            </div>
        </div>

    </div>
</div>
@endsection
@section("extra-js")

<script>
    $('.delete').on("click", function () {
        $('#delete-modal').modal('show');
        var id = ($('.delete').attr('val'));
        $('#del-id').attr('href','logo/'+id+'/del')
    });
    $(document).ready(function() {
        var table = $('#example').DataTable({
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
});
</script>
<script type="text/javascript" charset="utf8" src="/carousel/jquery-3.3.1.slim.min.js"/></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@endsection