@extends('layouts.app')
@section('content')
@include('includes.flashmessage')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Logo</div>
                {{ count($errors)==0 ? '' : fielderrors($errors) }}
                <div class="panel-body">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="logo-display  col-md-6">
                            <img src="{{ $logo->url }}"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::open(['method'=>'post','action'=>'Admin\LogoController@update','id'=>'form-id','class'=>'form-horizontal', 'files' => true]) !!}

                    <div class="form-group">
                        {!! Form::label('name', 'Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                        {!! Form::hidden('id', "{$logo->id}") !!}
                        <div class="col-sm-6">
                            {!! Form::text('name',"{$logo->name}",['class'=>'class form-control','id'=>'id']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                        <div class="col-sm-6"> 
                            {!! Form::submit('EDIT',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
