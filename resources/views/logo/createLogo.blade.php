@extends('layouts.app')

@section('content')
@include('includes.flashmessage')
<div class="container">
    <div class="row">
        @include('includes.sidebar')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Add Logo</div>
                {{ count($errors) == 0 ? '' : fielderrors($errors) }}
                <div class="panel-body">
                    {!! Form::open(['method'=>'post','action' => 'Admin\LogoController@store','id'=>'form-id','class'=>'form-horizontal','files' => true]) !!}

                        <div class="form-group">
                            {!! Form::label('name', 'Name',['id'=>'label-id','class'=>'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                            {!! Form::text('name',null,['class'=>'class form-control','id'=>'id']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('category_id','Category',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                           {!! Form::select('category_id',$category,null,['class'=>'form-control']) !!}
                            </div>
                        </div> 
                        <div class="form-group">
                            {!! Form::label('logo','Upload Logo',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                           {!! Form::file('logo',['id'=>'logo','class'=>'form-control','accept' => '.jpg, .png']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('zip','Upload ZIP File',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                           {!! Form::file('zip',['id'=>'zip','class'=>'form-control','accept' => '.zip']) !!}
                            </div>
                        </div> 
                        <div class="form-group">
                            {!! Form::label('for','Submit',['id'=>'label-id','class'=>'label-class col-sm-3 control-label']) !!}                       
                            <div class="col-sm-6"> 
                            {!! Form::submit('Add',['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
